# SEND REQUESTS TO A WEBSERVICE

**Client Side only, no back-end.**

## Features

* Bootstrap-themed pages
* Interface to view the clients data requesting and returning a JSON file throu the webservice.
* You can requet to Create, Delete, Edit and Search informations on database stored in the Webservice.
* You can do that requesting via GET or POST through this link: http://gui-request.herokuapp.com/
* The API is hosted on Heroku.

## General information:

* At this link: http://gui-request.herokuapp.com/visualizar/ you may see the Clients stored on database through a GET request.
* At this link: http://gui-request.herokuapp.com/incluir/ you can create new Clients sending a POST request.
* At this link: http://gui-request.herokuapp.com/pesquisar/ you can search Clients from its data.

## Built With:

* Python 3.6.4
* Django Framework 1.8.4 (using no Models, no Database, just Client Side, sending requests.)

## Contributing

If you want to contribute, just open an issue and tell me where I can improve.
Fork the repository and change whatever you'd like.
You can send Pull requests.

--------------------------------------------------------------------------------------------
