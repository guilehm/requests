from django import forms
from django.core.validators import MaxValueValidator, MinValueValidator


class ClienteForm(forms.Form):

    nome = forms.CharField(label='Nome', required=True)
    sobrenome = forms.CharField(label='Sobrenome', required=True)
    cpf = forms.CharField(label='CPF', required=True)
    opcoes = (('0', 'Masculino'),('1', 'Feminino'))
    sexo = forms.ChoiceField(widget=forms.RadioSelect(),choices=opcoes)
    idade = forms.IntegerField(
        validators=[MaxValueValidator(130),
                    MinValueValidator(0)
                    ]
    )

    def __init__(self, *args, **kwargs):
        super(ClienteForm, self).__init__(*args, **kwargs)
        self.fields['nome'].widget.attrs['class'] = 'form-control form-control-sm'
        self.fields['nome'].widget.attrs['id'] = 'id_nome_cliente'
        self.fields['nome'].widget.attrs['placeholder'] = 'Digite o nome'

        self.fields['sobrenome'].widget.attrs['class'] = 'form-control form-control-sm'
        self.fields['sobrenome'].widget.attrs['id'] = 'id_sobrenome_cliente'
        self.fields['sobrenome'].widget.attrs['placeholder'] = 'Digite o sobrenome'

        self.fields['cpf'].widget.attrs['class'] = 'form-control form-control-sm'
        self.fields['cpf'].widget.attrs['id'] = 'id_sobrenome_cliente'
        self.fields['cpf'].widget.attrs['placeholder'] = '___.___.___-__'

        self.fields['sexo'].widget.attrs['class'] = 'form-check-input'
        self.fields['sexo'].widget.attrs['id'] = 'id_sexo_cliente'

        self.fields['idade'].widget.attrs['class'] = 'form-control form-control-sm'
        self.fields['idade'].widget.attrs['id'] = 'id_idade_cliente'
        self.fields['idade'].widget.attrs['placeholder'] = 'Informe a idade'



class TermoPesquisa(forms.Form):

    pesquisa = forms.CharField(label='', required=True)

    def __init__(self, *args, **kwargs):
        super(TermoPesquisa, self).__init__(*args, **kwargs)
        self.fields['pesquisa'].widget.attrs['class'] = 'form-control form-control-sm w-50'
        self.fields['pesquisa'].widget.attrs['id'] = 'id_pesquisa_form'
        self.fields['pesquisa'].widget.attrs['placeholder'] = 'Procure...'