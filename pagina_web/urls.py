from django.conf.urls import url
from . import views

urlpatterns = [
    # Página inicial
    url(r'^$', views.index, name='index'),
    url(r'^visualizar/$', views.visualizar, name = 'visualizar'),
    url(r'^incluir/$', views.incluir, name = 'incluir'),
    url(r'deletar/(?P<cliente_id>\d+)/$', views.deletar, name='deletar'),
    url(r'editar/(?P<cliente_id>\d+)/$', views.editar, name='editar'),
    url(r'^pesquisar/$', views.pesquisar, name = 'pesquisar'),
]


