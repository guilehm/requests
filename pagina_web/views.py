from django.shortcuts import render
from .forms import ClienteForm
from .forms import TermoPesquisa
import requests
import json

# Create your views here.
def index(request):
    return render(request, 'pagina_web/index.html')

def visualizar(request):
    clientes = requests.get('http://gui-webservice.herokuapp.com/clientes-api/').json()
    context = {
        'clientes' : clientes,
    }
    return render(request, 'pagina_web/visualizar.html', context)

def pesquisar(request):

    if request.method == 'POST':
        pesquisar_form = TermoPesquisa(request.POST)
        if pesquisar_form.is_valid():
            pesquisa = pesquisar_form.cleaned_data['pesquisa']
            dicio = {'pesquisa' : pesquisa}
            print(pesquisa)
            print(dicio)
            json.dumps(dicio)
            r = requests.post('http://gui-webservice.herokuapp.com/clientes-api/pesquisar/', data=dicio).json()
            print(r)
            clientes = r
            pesquisado = True
            context = {
                'pesquisa': pesquisa,
                'pesquisar_form' : pesquisar_form,
                'clientes' : clientes,
                'pesquisado' : pesquisado
            }
            return render(request, 'pagina_web/pesquisar.html', context)
        else:
            pesquisar_form = TermoPesquisa()
            context = {
                'pesquisar_form': pesquisar_form,
            }
            return render(request, 'pagina_web/pesquisar.html', context)

    else:
        pesquisar_form = TermoPesquisa()
        context = {
            'pesquisar_form': pesquisar_form,
        }
        return render(request, 'pagina_web/pesquisar.html', context)

def incluir(request):
    if request.method == 'GET':
        incluir_form = ClienteForm()
        context = {
            'incluir_form' : incluir_form,
        }
        return render(request, 'pagina_web/incluir.html', context)

    elif request.method == 'POST':

        sucesso = False
        incluir_form = ClienteForm(request.POST)
        if incluir_form.is_valid():
            nome = incluir_form.cleaned_data['nome']
            sobrenome = incluir_form.cleaned_data['sobrenome']
            cpf = incluir_form.cleaned_data['cpf']
            sexo = incluir_form.cleaned_data['sexo']
            idade = incluir_form.cleaned_data['idade']
            dicio = {'nome':nome, 'sobrenome':sobrenome, 'cpf':cpf, 'sexo':sexo, 'idade':idade}
            json.dumps(dicio)
            r = requests.post('http://gui-webservice.herokuapp.com/clientes-api/', data=dicio)
            incluir_form = ClienteForm()
            sucesso = True
            context = {'sucesso' : sucesso, 'incluir_form' : incluir_form}
            return render(request, 'pagina_web/incluir.html', context)
        else:
            erro = True
            context = {'erro': erro, 'incluir_form': incluir_form}
            return render(request, 'pagina_web/incluir.html', context)


def deletar(request, cliente_id):
    r = requests.delete('http://gui-webservice.herokuapp.com/clientes-api/' + cliente_id)
    clientes = requests.get('http://gui-webservice.herokuapp.com/clientes-api/').json()
    context = {
        'clientes': clientes,
    }
    return render(request, 'pagina_web/visualizar.html', context)

def editar(request, cliente_id):
    r = requests.get('http://gui-webservice.herokuapp.com/clientes-api/' + cliente_id).json()
    if request.method == 'GET':
        incluir_form = ClienteForm(r)
        context = {'incluir_form' : incluir_form}
        return render(request, 'pagina_web/editar.html', context)

    elif request.method == 'POST':
        editar_form = ClienteForm(request.POST)
        if editar_form.is_valid():
            form = editar_form.cleaned_data
            print(form)
            req = requests.post('http://gui-webservice.herokuapp.com/clientes-api' + '/' + cliente_id + '/', data=form)
            print(req.text)
            editar_form = ClienteForm(request.POST)
            sucesso = True
            context = {'sucesso': sucesso, 'incluir_form': editar_form}
            return render(request, 'pagina_web/editar.html', context)

        else:
            editar_form = ClienteForm(request.POST)
            erro = True
            context = {'erro': erro, 'editar_form': editar_form}
            return render(request, 'pagina_web/editar.html', context)